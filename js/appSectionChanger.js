/*global tau */
function setupSectionChanger(changer_elem, indicator_elem) {
	var changer = document.getElementById("mainSectionChanger")
	var elPageIndicator = document.getElementById("mainPageIndicator"),
	pages = document.querySelectorAll("section").length;
	// make PageIndicator
	var pageIndicator = tau.widget.PageIndicator(document.getElementById(indicator_elem), {layout: "circular", numberOfPages: pages, maxPage: pages});
	// make SectionChanger object
	var sectionChanger = tau.widget.SectionChanger(document.getElementById(changer_elem), {
		circular: false,
		orientation: "horizontal"
	});
	
	pageIndicator.setActive(sectionChanger.getActiveSectionIndex());
	
	return sectionChanger, pageIndicator;
};


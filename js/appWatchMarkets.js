/*global tau*/

var loopRunning = false;
var startup = true;
var loopObj;
var sectionChanger;
var pageIndicator;

$("#main").on("pagebeforeshow", function () {
	
	if (tizen.preference.exists("appConfigured") && startup === true) {
		console.log("firing pagebeforeshow");
		$("#mainSections").append(generateStockSection("TST", "Test Shares LPO"));
		$("#mainSections").append(generateStockSection("OTH", "Other Shares LPO"));
		$("#mainSections").append(generateStockSection("EXT", "Extra Shares LPO"));
		$("#mainSections").append(generateStockAddSection());
		console.log($("#mainSectionChanger").html());
		sectionChanger, pageIndicator = setupSectionChanger("mainSectionChanger", "mainPageIndicator");
		startup = false;
	}
});

$("#main").on("pageshow", function () {
	if (!tizen.preference.exists("appConfigured")) {
		tau.changePage("setup.html");
	} else {
		startMainAppLoop();
	}
});

$("#main").on("pagehide", function () {
	if (tizen.preference.exists("appConfigured")) {
		sectionChanger.destroy();
		stopMainAppLoop();
	}
});

$("#mainSectionChanger").on("sectionchange", function (e) {
	pageIndicator.setActive(e.detail.active);
});

function generateStockSection(code, name) {
	var s_code = $("<div>", {"class": "stockelem-code"}).text(code);
	var s_name = $("<div>", {"class": "stockelem-name"}).text(name);
	var s_price = $("<div>", {"class": "stockelem-price stockelem-pricehold"}).text("$0.00");
	var s_change = $("<div>", {"class": "stockelem-change stockelem-pricehold"}).text("$0.00 (0.00%)");
	var s_low = $("<span>", {"class": "stockelem-low"}).text("L: $0.00");
	var s_high = $("<span>", {"class": "stockelem-high"}).text("H: $0.00");
	var s_open = $("<span>", {"class": "stockelem-open"}).text("O: $0.00");
	var s_close = $("<span>", {"class": "stockelem-close"}).text("C: $0.00");
	
	var s_stockelem = $("<div>", {"class": "stockelem"});
	//s_stockelem.dataset.stockCode=name;
	s_stockelem.append(s_code);
	s_stockelem.append(s_name);
	s_stockelem.append(s_price);
	s_stockelem.append(s_change);
	s_stockelem.append($("<div>").append(s_low).append(" ").append(s_high));
	s_stockelem.append($("<div>").append(s_open).append(" ").append(s_close));
	
	var s_innerdiv = $("<div>", {"class": "ui-stockelem-container"}).append(s_stockelem);
	var s_section = $("<section>", {"style": "text-align: center"}).append(s_innerdiv);
	
	return s_section;
}

function generateStockAddSection() {	
	var s_plusicon = $("<i>", {"class": "fas fa-icon fa-plus fa-lg"});
	var s_plusbtn = $("<button>", {"class": "ui-btn stockelem-addcodebtn"});
	var s_innerdiv = $("<div>", {"class": "ui-stockelem-container"});
	var s_section = $("<section>", {"style": "text-align: center"});
	
	s_plusbtn.append(s_plusicon);
	s_innerdiv.append(s_plusbtn);
	s_section.append(s_innerdiv);
	
	return s_section;
}

function mainAppLoop() {

}

function startMainAppLoop() {
	if (!loopRunning) {
		loopObj = setInterval(mainAppLoop, 500);
		loopRunning = true;
	}
}

function stopMainAppLoop() {
	if (loopRunning) {
		clearInterval(loopObj);
		loopRunning = false;
	}
}